Parametric Vase-Mode Storage Box
================================

This makes a rounded cubic box of a given width/depth (same value) and height.
The roundness of the corners is adjustable according to your preference.  There 
is also provision for a one-line label centered on one face, debossed into the surface.

Adjust the width, height, and roundness parameters at the top of the OpenSCAD
file. For the label, set the label, label_size, and label_depth parameters as
appropriate.  (label_depth should be kept to something less than your printer's
nozzle width.)

Uncomment either body() or lid() at the top, and render to STL.  Print
in spiral-vase mode with three bottom layers.

STLs for a 50x50x50mm box are included as an example.

This needs a fairly recent build of OpenSCAD to render.  I had been using a
build for 64-bit Windows from 6 January 2018, but was getting assertions on
render until I upgraded to the most recent build (24 January 2019, as of this
writing).  On Gentoo Linux, the fkmclane overlay has an ebuild that builds 
whatever's in the OpenSCAD GitHub.

Probably a wider nozzle would be more desirable for making strong prints.  Failing
that, another route to a stronger box would be to forgo vase mode and set 2 or more
perimeters and 0% infill in your slicer.
